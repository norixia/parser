# Import modules
import os
import csv
from xlrd import open_workbook

# TODO: you can write a function for user input and call it in the main()


def search_for_string(search_str):
    """
       Iterates over a specific file types within a given directory and searches for a given string.
    The file types are:
        - "txt"
        - "tsv"
        - "xls"
        - "xlsx"

    :param search_str: Search string input from the user.
    :return:
    """

    # Search path input
    # search_path = input("Enter a folder to search in: ")
    search_path = 'Test_Files'

    # File type input
    # file_type = 'txt'
    # file_types = ['txt', 'xls', 'xlsx', 'csv', 'tsv']
    file_types = ['txt', 'tsv', 'csv', 'xls', 'xlsx']

    # Search string input
    # search_str = input("Enter a string to start the search: ")
    # search_str = '26356300'

    # Append a directory separator unless it is already present
    if not (search_path.endswith("/") or search_path.endswith("\\")):
        search_path = search_path + "/"

    # If path does not exist, set search path to current directory
    if not os.path.exists(search_path):
        # search_path = os.getcwd()
        search_path = "."

    # i = 0
    # TODO: The output should print the file_name, line_nr and in the console
    # Check each file within the directory
    for file_name in os.listdir(path=search_path):

        # Iterate over the different file types
        for file_type in file_types:
            # Filter files by given type
            if file_name.endswith(file_type):
                if file_type in ['txt', 'tsv']:
                    search_in_text_formats(search_str, search_path, file_name)
                elif file_type == 'csv':
                    search_in_csv_format(search_str, search_path, file_name)
                elif file_type in ['xls', 'xlsx']:
                    search_in_excel_formats(search_str, search_path, file_name)
                else:
                    print("You are trying to search within another file format.")


def search_in_text_formats(search_str, search_path, file_name):
    """
    This function performs a search within the following file formats:
        - "txt"
        - "tsv"

    :param search_str: Search string input from the user.
    :param search_path: The path to the folder, in which a string search is to be executed.
    :param file_name: The name of the file currently in use.
    :return:
    """
    # i += 1
    # print(f'file_name {i}: {file_name}')
    # print('file_name {}: {}'.format(i, file_name))

    # Open file in reading mode
    with open(search_path + file_name, encoding="utf8") as file_opened:

        try:
            # Read the first line
            line = file_opened.readline()

            # Line number
            line_nr = 1

            # Iterate a search until the end of the file (EOF)
            while line != '':
                offset = line.find(search_str)
                # find() returns -1 on failure
                if offset != -1:
                    print(file_name, " [row:", line_nr, " , offset:", offset, "]: ", line)

                # Read next line
                line = file_opened.readline()

                # Line number incrementation
                line_nr += 1

        except UnicodeDecodeError:
            print("Decoding error occurred at file: ", file_name)


def search_in_csv_format(search_str, search_path, file_name):
    """
    This function performs a search within the following file formats:
        - "csv"

    :param search_str: Search string input from the user.
    :param search_path: The path to the folder, in which a string search is to be executed.
    :param file_name: The name of the file currently in use.
    :return:
    """
    # print("You are trying to search within a 'csv' file format.")

    # Read csv, and split on "," the line
    # with csv.reader(open(search_path + file_name), dialect='excel') as csv_reader:

    #     # Loop through csv list
    #     for row in csv_reader:
    #         # If current rows 2nd value is equal to input
    #         if search_str == row[1]:
    #             print(row)


def test_function():

    # print("tut:")
    # # Open a csv file in reading mode
    # with open("Test_Files/employee_birthday.csv") as csvfile:
    #     readcsv = csv.reader(csvfile, delimiter=',')
        
    #     names = []
    #     departments = []
    #     birthday_months = []

    #     # Print the csv file contents
    #     for row in readcsv:
    #         name = row[0]
    #         department = row[1]
    #         birthday_month = row[2]

    #         names.append(name)
    #         departments.append(department)
    #         birthday_months.append(birthday_month)

    #     print(names)
    #     print(departments)
    #     print(birthday_months)

    print("trial: ")
    # "Sniff" the format of a CSV file - i.e. delimiter, quotechar
    with open('Test_Files/employee_birthday.csv') as csvfile:
        # read the header row to determine to delimiter character
        sample = csvfile.readline()
        # has_header = csv.Sniffer().has_header(sample)
        # print(has_header)

        deduced_dialect = csv.Sniffer().sniff(sample)

    with open('Test_Files/employee_birthday.csv') as csvfile:
        reader = csv.reader(csvfile, dialect=deduced_dialect)
        # reader = csv.DictReader(csvfile, dialect=deduced_dialect)

        for i, row in enumerate(reader):
            print(row)
            if i >= 9:
                break

     
    
    # print("")
    # print("======== employee_birthday.csv : csv.reader(), ===========")
    # with open("Test_Files/employee_birthday.csv", 'r') as f1:
    #     csv_f1 = csv.reader(f1, delimiter=',')

    #     for i, row in enumerate(csv_f1):
    #         print(row)
    #         if i >= 3:
    #             break
    # print("")

    # print("======== employee_birthday.csv : csv.DictReader(), ===========")
    # with open("Test_Files/employee_birthday.csv", 'r') as f2:
    #     csv_f2 = csv.DictReader(f2, delimiter=',')

    #     for i, row in enumerate(csv_f2):
    #         print(dict(row))
    #         if i >= 3:
    #             break
    # print("")


def search_in_excel_formats(search_str, search_path, file_name):
    """
    This function performs a search within the following file formats:
        - "xls"
        - "xlsx"

    :param search_str: Search string input from the user.
    :param search_path: The path to the folder, in which a string search is to be executed.
    :param file_name: The name of the file currently in use.
    :return:
    """
    # print("You are trying to search within exel file format. Unfortunately, it is not yet implemented")

    # Open file in reading mode
    with open_workbook(search_path + file_name) as wb:

        # Read the first line
        try:
            for sheet in wb.sheets():
                number_of_rows = sheet.nrows
                number_of_columns = sheet.ncols

                for row in range(1, number_of_rows):

                    for column in range(number_of_columns):
                        value = sheet.cell(row, column).value

                        if search_str in str(value):
                            print("file_name: ", file_name)
                            print("value: ", value)
        except UnicodeDecodeError:
            print("Error at file: ", file_name)


def main():
    test_function()
    # print("+-------------------------------+")
    # print("| Welcome to the Search Console |")
    # print("+-------------------------------+\n")
    # print("")
    # search_str = input("I am looking for ... ")
    # while True:
    #     print("")

    #     if search_str.lower() in ['exit', 'esc', 'stop']:
    #         break
    #     else:
    #         print("")
    #         print("Search results: ")
    #         search_for_string(search_str)
    #         print("")
    #         user_input = input("Do you wish to search for something else? (yes/no): ")
    #         if user_input.lower() in ['no', 'n']:
    #             break
    #         elif user_input.lower() in ['yes', 'y']:
    #             search_str = input("I am looking for ... ")
    #         else:
    #             print("Unfortunately, your answer is not valid. The program ends.")
    #             break

    # print("THE END!")


if __name__ == '__main__':
    main()
